'use strict'

Polymer({
    color: undefined
    dragData: undefined

    observe:
        color: 'colorChanged'

    publish:
        color:
            value: 'rgba(255,0,0)'
            reflect: true

    ready: () ->
        setTimeout =>
            @draw(null, @color)
        , 0

    colorChanged: (_, color) ->
        @draw(_, color)

    draw: (_, color) ->
        canvas = @$.canvas

        width = canvas.width = canvas.offsetWidth
        height = canvas.height = canvas.offsetHeight

        context = canvas.getContext('2d')

        context.clearRect 0, 0, width, height

        context.fillStyle = color
        context.fillRect 0, 0, width, height

    selectColor: (event) ->
        #

    dragColor: (event, detail, sender) ->
        if event.x > 0 || event.y > 0
            @dragData = {
                x: event.x
                y: event.y
            }

        event.dataTransfer.allowedEffect = 'copy'
        event.dataTransfer.setData('text/plain', @color)

    dropColor: (event, detail, sender) ->
        # ISSUE: Don't work for browser security reasons.
        # dragColor = event.dataTransfer.getData('text/plain')

        # TODO: identify hueColor for color

        dragElement = event.target

        try
            x = dragElement.dragData.x
            y = dragElement.dragData.y

            # HACK: `event.relatedTarget` don't exist, so we have to get element by x,y
            dropElement = event.relatedTarget || document.elementFromPoint(x, y)

            # debug
            # @debugDot(x, y, 'green', 12)

            # debug
            # console.log {
            #     'dragColor': dragElement.color
            #     'dropColor': dropElement.color
            #     'dragElement': dragElement
            #     'dropElement': dropElement
            # }

            dropElement.color = dragElement.color

    log: () ->
        console.log.apply console, arguments

    # debug
    debugEvent: (event, detail, sender) ->
        console.log "EVENT:", event.type, event, event.target, event.relatedTarget

    # debug
    debugDot: (x, y, color = 'red', size = 3) ->
        # console.log '.', x, y
        dot = document.createElement('div')
        dot.style.position = 'absolute'
        dot.style.left = x + 'px'
        dot.style.top = y + 'px'
        dot.style.backgroundColor = color
        dot.style.borderRadius = '50%'
        dot.style.width = dot.style.height = "#{size}px"
        dot.style.pointerEvents = 'none'
        dot.style.opacity = '0.8'
        dot.style.zIndex = 100000
        document.body.appendChild(dot)

})