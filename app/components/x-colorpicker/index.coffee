'use strict'

Polymer({
    color: undefined
    hueColor: undefined
    defaultColor: 'red'
    dragData: {}

    observe:
        hueColor: 'createColorSpectrum'
        color: 'createColor'

    publish:
        color:
            value: 'rgba(255,0,0)'
            reflect: true

    ready: () ->
        @createColorsSpectrum()

        this.hueColor ?= this.defaultColor
        this.color ?= this.defaultColor

    createColorsSpectrum: () ->
        canvas = @$.colorsSpectrum

        width = canvas.width # = canvas.parentElement.offsetWidth #= canvas.offsetWidth
        height = canvas.height # = canvas.parentElement.offsetHeight #= canvas.offsetHeight

        # console.log 'x', canvas.parentElement.offsetWidth, canvas.parentElement.parentElement.offsetHeight
        # console.log width ,canvas.width ,canvas.offsetWidth, canvas.style, canvas.parentElement.offsetWidth

        context = canvas.getContext('2d')

        hueGradient = context.createLinearGradient(0, 0, 0, height)
        [
            [0/6, "rgb(255,0,0)"]
            [1/6, "rgb(255,0,255)"]
            [2/6, "rgb(0,0,255)"]
            [3/6, "rgb(0,255,255)"]
            [4/6, "rgb(0,255,0)"]
            [5/6, "rgb(255,255,0)"]
            [6/6, "rgb(255,0,0)"]
        ].forEach (colorStop) ->
            hueGradient.addColorStop colorStop[0], colorStop[1]

        context.fillStyle = hueGradient
        context.fillRect 0, 0, width, height

    createColorSpectrum: (_, color) ->
        canvas = @$.colorSpectrum

        # @log $(canvas).width(), $(canvas).outerWidth(), $(canvas).innerWidth(), $(canvas).css('width'), canvas.width

        width = canvas.width #= canvas.parentElement.offsetWidth # canvas.offsetWidth
        height = canvas.height #= canvas.parentElement.offsetHeight # canvas.offsetHeight

        context = canvas.getContext('2d')

        context.clearRect 0, 0, width, height

        context.fillStyle = color
        context.fillRect 0, 0, width, height

        whiteGradient = context.createLinearGradient(0, 0, width, 0)
        whiteGradient.addColorStop 0, "white"
        whiteGradient.addColorStop 1, "transparent"

        context.fillStyle = whiteGradient
        context.fillRect 0, 0, width, height

        blackGradient = context.createLinearGradient(0, 0, 0, height)
        blackGradient.addColorStop 0, "transparent"
        blackGradient.addColorStop 1, "black"

        context.fillStyle = blackGradient
        context.fillRect 0, 0, width, height

    createColor: (_, color) ->
        canvas = @$.color

        width = canvas.width # = canvas.parentElement.offsetWidth # canvas.offsetWidth
        height = canvas.height # = canvas.parentElement.offsetHeight # canvas.offsetHeight

        context = canvas.getContext('2d')

        context.clearRect 0, 0, width, height

        context.fillStyle = color
        context.fillRect 0, 0, width, height

    selectHueColor: (event) ->
        mouseDown = event.which is 1 # BUG: always true in FF

        if ~navigator.userAgent.indexOf('Firefox')
            mouseDown = event.buttons is 1

        if mouseDown
            @hueColor = @getColorAtXY(@$.colorsSpectrum, event.pageX, event.pageY)

    selectColor: (event) ->
        mouseDown = event.which is 1 # BUG: always true in FF

        if ~navigator.userAgent.indexOf('Firefox')
            mouseDown = event.buttons is 1

        if mouseDown
            @color = @getColorAtXY(@$.colorSpectrum, event.pageX, event.pageY)

    getColorAtXY: (canvas, x, y) ->
        component = this
        frame = canvas.parentElement

        x = x - component.offsetLeft - frame.offsetLeft
        y = y - component.offsetTop - frame.offsetTop

        @rgb = @getPixelData(canvas, x, y)

        # debug
        # @debugDot(x, y, 'green')

        @hsl = @getHSLFromRGB(@rgb)

        console.log @hsl, @rgb

        return @getRGBStringFromRGB(@rgb)

    getPixelData: (canvas, x, y) ->
        return @getRGBFromPixel(canvas.getContext('2d').getImageData(x, y, 1, 1).data)

    getRGBFromPixel: (pixelData) ->
        return [pixelData[0], pixelData[1], pixelData[2]]

    getRGBStringFromRGB: (rgb) ->
        return "rgb(#{ rgb.join(",") })"

    getHSLFromRGB: (rgb) ->
        # https://github.com/harthur/color
        # http://old.driven-by-data.net/about/chromajs/#/1
        r = rgb[0] / 255
        g = rgb[1] / 255
        b = rgb[2] / 255

        max = Math.max(r, g, b)
        min = Math.min(r, g, b)
        h = 0
        s = 0
        l = (max + min) / 2

        if max != min
            d = max - min
            if l > 0.5
                s = d / (2 - max - min)
            else
                s = d / (max + min)

            switch max
                when r then h = (g - b) / d + (g < b ? 6 : 0)
                when g then h = (b - r) / d + 2
                when b then h = (r - g) / d + 4

            h = h / 6

        return [h, s, l]

    getColorsSpectrumXYFromRGB: () ->
        # TODO: Calculate X/Y from a color

    getColorSpectrumXYFromRGB: () ->
        # TODO: Calculate X/Y from a color

    dragColor: (event, detail, sender) ->
        if event.x > 0 || event.y > 0
            @dragData = {
                x: event.x
                y: event.y
            }

        event.dataTransfer.allowedEffect = 'copy'
        event.dataTransfer.setData('text/plain', @color)

    dropColor: (event, detail, sender) ->
        # ISSUE: Don't work for browser security reasons.
        # dragColor = event.dataTransfer.getData('text/plain')

        # TODO: identify hueColor for color

        dragElement = event.target

        try
            x = dragElement.dragData.x
            y = dragElement.dragData.y

            # HACK: `event.relatedTarget` don't exist, so we have to get element by x,y
            dropElement = event.relatedTarget || document.elementFromPoint(x, y)

            # debug
            # @debugDot(x, y, 'green', 12)

            # debug
            # console.log {
            #     'dragColor': dragElement.color
            #     'dropColor': dropElement.color
            #     'dragElement': dragElement
            #     'dropElement': dropElement
            # }

            dropElement.color = dragElement.color

    # closestElement: (el, selector) ->
    #     matchesSelector = el.matches || el.webkitMatchesSelector || el.mozMatchesSelector || el.msMatchesSelector
    #     selector = selector.toLowerCase()

    #     while el
    #         if matchesSelector.bind(el)(selector)
    #             return el
    #         else
    #             el = el.parentElement

    #     return undefined

    log: () ->
        console.log.apply console, arguments

    # debug
    debugEvent: (event, detail, sender) ->
        console.log "EVENT:", event.type, event, event.target, event.relatedTarget

    # debug
    debugDot: (x, y, color = 'red', size = 3) ->
        # console.log '.', x, y
        dot = document.createElement('div')
        dot.style.position = 'absolute'
        dot.style.left = x + 'px'
        dot.style.top = y + 'px'
        dot.style.backgroundColor = color
        dot.style.borderRadius = '50%'
        dot.style.width = dot.style.height = "#{size}px"
        dot.style.pointerEvents = 'none'
        dot.style.opacity = '0.8'
        dot.style.zIndex = 100000
        document.body.appendChild(dot)

})
