'use strict'

Polymer({
    base: undefined
    color: undefined
    hsl: undefined
    rgb: undefined
    x: undefined
    y: undefined
    rendered: false
    width: undefined
    height: undefined

    observe:
        base: 'onBaseChanged'
        color: 'onColorChanged'
        x: 'onSelectionChanged'
        y: 'onSelectionChanged'
        rendered: 'onSelectionChanged'

    publish:
        base:
            value: 'rgb(255,0,0)'
            reflect: true
        color:
            value: undefined
            reflect: true
        hsl:
            value: undefined
            reflect: true
        rgb:
            value: undefined
            reflect: true

    ready: () ->
        # HACK: Required when rendered inside another Shadow DOM...don't know why.
        setTimeout =>
            @draw(null, @base)
        , 0

    onBaseChanged: (_, color) ->
        @draw(_, color)
        @onSelectionChanged()

    onColorChanged: (_, value) ->
        if _ != value
            @onSelectionChanged()

    onSelectionChanged: () ->
        if @rendered
            if typeof @x == 'number' && typeof @y == 'number'
                @rgb = @getRGBFromXY(@$.canvas, @x, @y)
            else
                @rgb = @getRGBFromRGBString(@color || @base)
                xy = @getXYFromRGB(@rgb)
                @x = xy[0]
                @y = xy[1]

        if @rgb
            @hsl = @getHSLFromRGB(@rgb)
            @color = @getRGBStringFromRGB(@rgb)

    onSelectColor: (event) ->
        mouseDown = event.which is 1 # BUG: always true in FF

        if ~navigator.userAgent.indexOf('Firefox')
            mouseDown = event.buttons is 1

        if mouseDown
            x = event.x
            y = event.y

            component = this
            frame = @$.canvas.parentElement

            @x = x - component.offsetLeft - frame.offsetLeft
            @y = y - component.offsetTop - frame.offsetTop

    getRGBFromXY: (canvas, x, y) ->
        # Adjust one pixel to avoid issue with black pixel from canvas edge.
        if x >= canvas.width
            x = canvas.width - 1

        if y >= canvas.height
            y = canvas.height - 1

        rgb = @getPixelData(canvas, x, y)
        return rgb

    getPixelData: (canvas, x, y) ->
        return @getRGBFromPixel(canvas.getContext('2d').getImageData(x, y, 1, 1).data)

    getRGBFromPixel: (pixelData) ->
        return [pixelData[0], pixelData[1], pixelData[2]]

    getRGBStringFromRGB: (rgb) ->
        return "rgb(#{ rgb.join(",") })"

    # TODO: Handle parsing any CSS color string.
    getRGBFromRGBString: (rgbString) ->
        return rgbString.split(/[\(\)]/)[1].split(',').map (value) -> parseInt(value)

    getHSVFromRGB: (rgb) ->
        rr = undefined
        gg = undefined
        bb = undefined

        r = rgb[0] / 255
        g = rgb[1] / 255
        b = rgb[2] / 255

        h = undefined
        s = undefined

        v = Math.max(r, g, b)
        diff = v - Math.min(r, g, b)

        diffc = (c) ->
            (v - c) / 6 / diff + 1 / 2

        if diff == 0
            h = s = 0
        else
            s = diff / v
            rr = diffc(r)
            gg = diffc(g)
            bb = diffc(b)

        if r == v
            h = bb - gg
        else if g == v
            h = (1 / 3) + rr - bb
        else if b == v
            h = (2 / 3) + gg - rr

        if h < 0
            h += 1
        else if h > 1
            h -= 1

        return [Math.round(h * 360), Math.round(s * 100), Math.round(v * 100)]

    getHSLFromRGB: (rgb) ->
        # https://github.com/harthur/color
        # http://old.driven-by-data.net/about/chromajs/#/1
        r = rgb[0] / 255
        g = rgb[1] / 255
        b = rgb[2] / 255

        max = Math.max(r, g, b)
        min = Math.min(r, g, b)
        h = 0
        s = 0
        l = (max + min) / 2

        if max != min
            d = max - min
            if l > 0.5
                s = d / (2 - max - min)
            else
                s = d / (max + min)

            switch max
                when r then h = (g - b) / d + (g < b ? 6 : 0)
                when g then h = (b - r) / d + 2
                when b then h = (r - g) / d + 4

            h = h / 6

        return [Math.round(h * 360), Math.round(s * 100), Math.round(l * 100)]

    log: () ->
        console.log.apply console, arguments

    # debug
    debugEvent: (event, detail, sender) ->
        console.log "EVENT:", event.type, event, event.target, event.relatedTarget

    # debug
    debugDot: (x, y, color = 'red', size = 3) ->
        # console.log '.', x, y
        dot = document.createElement('div')
        dot.style.position = 'absolute'
        dot.style.left = x + 'px'
        dot.style.top = y + 'px'
        dot.style.backgroundColor = color
        dot.style.borderRadius = '50%'
        dot.style.width = dot.style.height = "#{size}px"
        dot.style.pointerEvents = 'none'
        dot.style.opacity = '0.8'
        dot.style.zIndex = 100000
        document.body.appendChild(dot)

    getXYFromRGB: (rgb) ->
        hsl = @getHSLFromRGB(rgb)
        hsv = @getHSVFromRGB(rgb)

        s = hsv[1] # x
        l = hsv[2] # y

        x = (s / 100.0) * @width
        y = (1.0 - l / 100.0) * @height

        # TODO: calculate X/Y correctly based on SL
        # http://bgrins.github.io/TinyColor/
        # http://codeitdown.com/hsl-hsb-hsv-color/

        return [x, y]

    draw: (_, color) ->
        canvas = @$.canvas

        @width = canvas.width = canvas.offsetWidth
        @height = canvas.height = canvas.offsetHeight

        context = canvas.getContext('2d')

        context.clearRect 0, 0, @width , @height

        context.fillStyle = color
        context.fillRect 0, 0, @width , @height

        whiteGradient = context.createLinearGradient(0, 0, @width , 0)
        whiteGradient.addColorStop 0, "white"
        whiteGradient.addColorStop 1, "transparent"

        context.fillStyle = whiteGradient
        context.fillRect 0, 0, @width , @height

        blackGradient = context.createLinearGradient(0, 0, 0, @height)
        blackGradient.addColorStop 0, "transparent"
        blackGradient.addColorStop 1, "black"

        context.fillStyle = blackGradient
        context.fillRect 0, 0, @width , @height

        # ensure canvas is painted before consider it rendered.
        setTimeout =>
            @rendered = true
        , 0

})