'use strict'

# require.js + polymer.js
# https://gist.github.com/tantaman/7222599
# https://github.com/brianchirls/Seriously.js/tree/master/effects

class DefaultPalette
    constructor: () ->
        # setup

    draw: (canvas, attributes) ->
        # draw
        width = canvas.width = canvas.offsetWidth
        height = canvas.height = canvas.offsetHeight

        context = canvas.getContext('2d')

        context.clearRect 0, 0, width, height

        context.fillStyle = attributes.color
        context.fillRect 0, 0, width, height

global = this

Polymer({
    publish:
        palette:
            value: 'Default'
            reflect: true
        invalid:
            value: false
            reflect: true

    observe:
        palette: 'paletteChanged'

    ready: ->
        @log 'ready'

    paletteChanged: (_, value) ->
        @log 'paletteChanged', value
        @draw()

    getPalette: (palette) ->
        palette ?= @palette

        if typeof palette == 'string' and palette.length
            Palette = global[palette]

            if typeof Palette == 'function'
                palette = new Palette()

                if typeof palette.draw == 'function'
                    return palette
                    palette.draw(canvas, this)
                    return @invalid

    validatePalette: (palette) ->
        palette ?= @palette

        return @invalid = !@getPalette(palette)

    draw: (palette) ->
        palette ?= @palette

        if @validatePalette(palette)
            p = @getPalette(palette)
            p.draw(canvas, @)

    log: () ->
        console.log.apply console, arguments

        # debug
    debugEvent: (event, detail, sender) ->
        console.log "EVENT:", event.type, event, event.target, event.relatedTarget

    # debug
    debugDot: (x, y, color = 'red', size = 3) ->
        # console.log '.', x, y
        dot = document.createElement('div')
        dot.style.position = 'absolute'
        dot.style.left = x + 'px'
        dot.style.top = y + 'px'
        dot.style.backgroundColor = color
        dot.style.borderRadius = '50%'
        dot.style.width = dot.style.height = "#{size}px"
        dot.style.pointerEvents = 'none'
        dot.style.opacity = '0.8'
        dot.style.zIndex = 100000
        document.body.appendChild(dot)

})