'use strict'

# PATTERN:
#
#   Parsing:
#       <TYPE> - <THREADS> - <THICKNESS> - <COLORS> - <PATTERN>
#
#   Examples:
#       stripes-18-1-1-8a
#       stripes-18-1-2-8a.8b
#       stripes-17-1-2-4a.9b.4a
#       stripes-17-1-2-4b.9a.4b
#       stripes-17-1-3-4a.4b.1c.4b.4a
#       stripes-17-1-3-4c.4b.1c.4b.4c
#       stripes-17-1-3-4c.4b.1a.4b.4c
#       stripes-17-1-2-7a.3b.7a
#       stripes-17-1-2-7b.3a.7b
#       stripes-17-1-2-10a.3b.4a
#       stripes-17-1-2-7a.3c.7a

# stripes-17-1-3-4a.4b.1c.4b.4a
# stripes-17-1-3-aaaa.bbbb.c.bbbb.aaaa

Polymer({
    pattern: undefined
    threads: undefined

    observe:
        design: 'onPatternChanged'
        colors: 'onColorsChanged'

    publish:
        pattern:
            value: 'stripes-18-1-1-8a'
            reflect: true
        colors:
            value: 'rgb(0,0,0), rgb(255,0,0)'
            reflect: false

    ready: () ->
        # console.log "#{this.tagName} created", @pattern

        setTimeout =>
            @onPatternChanged()
            @onColorsChanged()
        , 0

    onPatternChanged: (_, value) ->
        @parsePattern()

    onColorsChanged: (_, value) ->
        @parseColors()

    parsePattern: ->
        if !@pattern then return

        parts = @pattern.split('-')
        @_pattern =
            type: parts[0]
            threadCount: parseInt(parts[1])
            threadThickness: parseInt(parts[2])
            colorCount: parseInt(parts[3])
            structure: parts[4]

        @_pattern.threads = []

        @_pattern.structure.split('.').forEach (part) =>
            partThickness = parseInt(part)

            for i in [1..partThickness]
                thread = {}
                thread.thickness = @_pattern.threadThickness
                thread.colorCode = part.match(/[a-z]/i)[0]

                @_pattern.threads.push thread

        # console.log 'parsePattern', @_pattern

    parseColors: ->
        if !@colors then return


        colorStrings = @colors.split(',')
            .map (colorString) -> colorString.trim()

        @_colors = colorStrings
            # .map (colorString) ->
            #     console.log colorString
            #     if !!~colorString.indexOf('#')
            #         colorString.split(/[\(\)]/gmi)[1].split(',').map (v) -> parseInt(v)

        # console.log 'parseColors', @_colors
})

