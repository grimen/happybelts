'use strict'
gulp = require("gulp")
util = require('gulp-util')
bower = require('bower')
plugins = require('gulp-load-plugins')()
_ = require('lodash')
say = require('say')

# PIPES

plumber = plugins.plumber
rename = plugins.rename
clean = plugins.rimraf
ignore = plugins.ignore
connect = plugins.connect

# PATHS

paths = {
    coffee: './app/**/*.coffee'
    jade: './app/**/*.jade'
    styl: [
        './app/**/*.styl'
        '!./app/vendor/**'
    ]
    bower: './app/bower/**'
    target: './dist'
    assets: {
        all: ['./app/assets/**']
        images: './app/assets/images'
    }
}

filters = {
    coffee: plugins.filter("**/*.styl")
    jade: plugins.filter("**/*.jade")
    styl: plugins.filter("**/*.styl")
}

options = {
    coffee: {
        bare: true
    }
    jade: {
        pretty: true
    }
    styl: {
        errors: true
        cache: false
        paths: []
        set: []
        use: [require('nib')()]
    }
    plumber: {
        errorHandler: (err) ->
            say.speak 'Bubbles', "FAIL"
            console.log err.message
    }
}

# MAIN

gulp.task 'build', ['stylus', 'coffee', 'jade', 'bower', 'assets']
gulp.task 'default', ['build']

gulp.task 'clean', () ->
    gulp.src('./dist', { read: false })
        # .pipe(ignore('node_modules/**'))
        .pipe(clean())

# CORE

gulp.task 'bower', ()->
    gulp.src(paths.bower)
        .pipe(gulp.dest("#{paths.target}/bower"))

gulp.task 'assets', ()->
    gulp.src(paths.assets.all)
        .pipe(gulp.dest("#{paths.target}/assets"))

gulp.task 'coffee', (done) ->
    coffee = plugins.coffee(options.coffee).on('error', plugins.util.log)

    gulp.src(paths.coffee)
        .pipe(plumber(options.plumber))
        .pipe(coffee)
        .pipe(connect.reload())
        .pipe(gulp.dest(paths.target))
        # .on('end', done)

gulp.task 'jade', (done) ->
    jade = plugins.jade(options.jade).on('error', plugins.util.log)

    gulp.src(paths.jade)
        .pipe(plumber(options.plumber))
        .pipe(jade)
        .pipe(connect.reload())
        .pipe(gulp.dest(paths.target))
        # .on('end', done)

gulp.task 'stylus', ['styl']
gulp.task 'styl', (done) ->
    stylus = plugins.stylus(options.styl).on('error', plugins.util.log)

    gulp.src(paths.styl)
        .pipe(plumber(options.plumber))
        .pipe(stylus)
        .pipe(rename({ extname: '.css' }))
        .pipe(connect.reload())
        .pipe(gulp.dest(paths.target))
        # .on('end', done)

gulp.task 'watch', ['build', 'connect'], () ->
    gulp.watch(paths.coffee, ['coffee'])
    gulp.watch(paths.jade, ['jade'])
    gulp.watch(paths.styl, ['styl'])

gulp.task "connect", () ->
    return plugins.connect.server({
        root: [paths.target],
        port: 5555,
        livereload: false,
        hostname: 'localhost',
        middleware: (connect, options) ->
            return [
                (req, res, next) ->
                    res.setHeader('Access-Control-Allow-Origin', '*')
                    res.setHeader('Access-Control-Allow-Methods', '*')
                    return next()
            ]
    })

