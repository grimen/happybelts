
all: watch

clean:
	rm -rf ./app/bower/*

install:
	npm install && ./node_modules/.bin/bower install

update:
	npm update && ./node_modules/.bin/bower update

build:
	./node_modules/.bin/gulp build

watch:
	./node_modules/.bin/gulp watch

.PHONY: clean install build watch
